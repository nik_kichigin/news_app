package com.example.user.news_app.data.network;

import com.example.user.news_app.data.network.res.GetNewsArticlesRes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestService {

    @GET("top-headlines")
    Call<GetNewsArticlesRes> getNewsArticles (@Query("country") String country,
                                              @Query("category") String category,
                                              @Query("apiKey") String apiKey);
}
