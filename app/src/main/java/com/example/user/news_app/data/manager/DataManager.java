package com.example.user.news_app.data.manager;

import com.example.user.news_app.data.network.RestService;
import com.example.user.news_app.data.network.ServiceGenerator;
import com.example.user.news_app.data.network.res.GetNewsArticlesRes;

import retrofit2.Call;

public class DataManager {
    private static DataManager INSTANCE = null;
    private RestService mRestService;

    public DataManager(){
        this.mRestService = ServiceGenerator.createService(RestService.class);
    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public Call<GetNewsArticlesRes> getNewsArticles(String country, String category, String apiKey) {
        return mRestService.getNewsArticles(country, category, apiKey);
    }
}
