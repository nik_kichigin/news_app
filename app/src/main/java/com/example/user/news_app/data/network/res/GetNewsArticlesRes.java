package com.example.user.news_app.data.network.res;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetNewsArticlesRes {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("totalResults")
    @Expose
    public int totalResults;
    @SerializedName("articles")
    @Expose
    public List<Article> articles = null;

    public String getStatus() {
        return status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public static class Article implements Parcelable{

        @SerializedName("source")
        @Expose
        public Source source;
        @SerializedName("author")
        @Expose
        public Object author;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("urlToImage")
        @Expose
        public String urlToImage;
        @SerializedName("publishedAt")
        @Expose
        public String publishedAt;

        protected Article(Parcel in) {
            title = in.readString();
            description = in.readString();
            url = in.readString();
            urlToImage = in.readString();
            publishedAt = in.readString();
        }

        public static final Parcelable.Creator<Article> CREATOR = new Parcelable.Creator<Article>() {
            @Override
            public Article createFromParcel(Parcel in) {
                return new Article(in);
            }

            @Override
            public Article[] newArray(int size) {
                return new Article[size];
            }
        };

        public Source getSource() {
            return source;
        }

        public Object getAuthor() {
            return author;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getUrl() {
            return url;
        }

        public String getUrlToImage() {
            return urlToImage;
        }

        public String getPublishedAt() {
            return publishedAt;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(title);
            dest.writeString(description);
            dest.writeString(url);
            dest.writeString(urlToImage);
            dest.writeString(publishedAt);
        }
    }

    public class Source {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("name")
        @Expose
        public String name;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}
