package com.example.user.news_app.presenters;

import com.example.user.news_app.interfaces.ItemArticleInterface;
import com.example.user.news_app.interfaces.base.BasePresenter;

public class ItemArticlePresenter extends BasePresenter<ItemArticleInterface.View> implements ItemArticleInterface.Presenter {
}
