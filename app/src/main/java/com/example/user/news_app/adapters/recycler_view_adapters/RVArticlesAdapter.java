package com.example.user.news_app.adapters.recycler_view_adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.user.news_app.R;
import com.example.user.news_app.data.network.res.GetNewsArticlesRes.Article;
import com.example.user.news_app.views.ItemArticleActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RVArticlesAdapter extends RecyclerView.Adapter<RVArticlesAdapter.CardViewHolder>{

    class CardViewHolder extends RecyclerView.ViewHolder  {
        CardView cv;
        ImageView articleImage;
        TextView acticleTitle, articlesTime;

        CardViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.article_card);
            articleImage = itemView.findViewById(R.id.article_image);
            acticleTitle = itemView.findViewById(R.id.article_title);
            articlesTime = itemView.findViewById(R.id.article_time);

            cv.setOnClickListener(v -> {

                Article article = articles.get(getAdapterPosition());

                Intent intent = new Intent(context, ItemArticleActivity.class);
                intent.putExtra(Article.class.getCanonicalName(), article);
                context.startActivity(intent);

            });
        }
    }

    private List<Article> articles;
    private Context context;
    public RVArticlesAdapter(List<Article> articles, Context context){
        this.articles = articles;
        this.context = context;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_article, viewGroup, false);
        CardViewHolder cvh = new CardViewHolder(v);

        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CardViewHolder cardViewHolder, int i) {

        Article article = articles.get(i);

        cardViewHolder.acticleTitle.setText(article.getTitle());
        Glide.with(context).load(article.getUrlToImage()).into(cardViewHolder.articleImage);

        String date = article.getPublishedAt();

        long longDate = 1;

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            Date d = f.parse(date);
            longDate = d.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        cardViewHolder.articlesTime.setText(new java.text.SimpleDateFormat("MM.dd HH:mm", Locale.ENGLISH).format(longDate));
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
