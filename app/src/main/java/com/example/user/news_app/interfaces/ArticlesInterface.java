package com.example.user.news_app.interfaces;

import android.content.Context;

import com.example.user.news_app.adapters.recycler_view_adapters.RVArticlesAdapter;
import com.example.user.news_app.data.manager.DataManager;
import com.example.user.news_app.interfaces.base.BaseInterfacePresenter;
import com.example.user.news_app.interfaces.base.BaseInterfaceView;

public interface ArticlesInterface {
    interface View extends BaseInterfaceView {
        void showMessage(int messageResId);
        void setArticlesToUI(RVArticlesAdapter adapter);
    }

    interface Presenter extends BaseInterfacePresenter<View> {
        void getArticlesRequest(Context context);
        void searchTextChanged(String text);
        void restoreRV(Context context);
    }
}
