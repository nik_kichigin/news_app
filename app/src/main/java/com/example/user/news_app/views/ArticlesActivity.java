package com.example.user.news_app.views;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.example.user.news_app.R;
import com.example.user.news_app.adapters.recycler_view_adapters.RVArticlesAdapter;
import com.example.user.news_app.interfaces.ArticlesInterface;
import com.example.user.news_app.presenters.ArticlesPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticlesActivity extends AppCompatActivity implements ArticlesInterface.View {

    ArticlesPresenter presenter;

    @BindView(R.id.articles_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.idToolbar)
    Toolbar toolbar;

    @BindView(R.id.search_edit_text)
    EditText editTextSearch;

    @BindView(R.id.root_layout)
    CoordinatorLayout rootView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.articles_activity);

        ButterKnife.bind(this);

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = editTextSearch.getText().toString();
                presenter.searchTextChanged(text);
            }
        });

        presenter = (ArticlesPresenter) getLastCustomNonConfigurationInstance();
    }

    private void initPresenter(){
        presenter = new ArticlesPresenter();
        presenter.attachView(this);
        presenter.getArticlesRequest(this);
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
                presenter.restoreRV(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return presenter;
    }

    @Override
    protected void onResume() {
        super.onResume();
        attachView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        detachView();
    }

    @Override
    public void showMessage(int messageResId) {
        Snackbar snackbar = Snackbar.make(rootView, messageResId, Snackbar.LENGTH_LONG);
        snackbar.setDuration(1500);
        snackbar.show();
    }

    @Override
    public void setArticlesToUI(RVArticlesAdapter adapter) {

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);

    }
}
