package com.example.user.news_app.presenters;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.user.news_app.R;
import com.example.user.news_app.adapters.recycler_view_adapters.RVArticlesAdapter;
import com.example.user.news_app.data.manager.DataManager;
import com.example.user.news_app.data.network.res.GetNewsArticlesRes;
import com.example.user.news_app.data.network.res.GetNewsArticlesRes.Article;
import com.example.user.news_app.interfaces.ArticlesInterface;
import com.example.user.news_app.interfaces.base.BasePresenter;
import com.example.user.news_app.utils.AppConfig;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticlesPresenter extends BasePresenter<ArticlesInterface.View> implements ArticlesInterface.Presenter {

    private List<Article> allArticles;
    private List<Article> adapterArticles = new ArrayList<>();
    private RVArticlesAdapter adapter;
    private DataManager mDataManager;


    @Override
    public void getArticlesRequest(Context context) {

        mDataManager = DataManager.getInstance();

        Call<GetNewsArticlesRes> call = mDataManager.getNewsArticles("ru", "business", AppConfig.ApiKey);
        call.enqueue(new Callback<GetNewsArticlesRes>() {
            @Override
            public void onResponse(@NonNull Call<GetNewsArticlesRes> call, @NonNull Response<GetNewsArticlesRes> response) {

                allArticles = response.body().getArticles();
                adapterArticles.addAll(allArticles);

                adapter = new RVArticlesAdapter(adapterArticles, context);
                getView().setArticlesToUI(adapter);
            }

            @Override
            public void onFailure(@NonNull Call<GetNewsArticlesRes> call, @NonNull Throwable t) {
                getView().showMessage(R.string.request_error);
            }
        });

    }

    @Override
    public void searchTextChanged(String text) {

        adapterArticles.clear();

        for (int i = 0; i < allArticles.size(); i ++) {
            if (allArticles.get(i).getTitle().toLowerCase().contains(text.toLowerCase())) {
                adapterArticles.add(allArticles.get(i));
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void restoreRV(Context context) {
        if (adapter != null) {
            adapter.setContext(context);
            getView().setArticlesToUI(adapter);
        }
    }
}
