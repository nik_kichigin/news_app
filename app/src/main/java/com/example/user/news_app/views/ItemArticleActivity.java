package com.example.user.news_app.views;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.example.user.news_app.R;
import com.example.user.news_app.data.network.res.GetNewsArticlesRes.Article;
import com.example.user.news_app.interfaces.ItemArticleInterface;
import com.example.user.news_app.presenters.ItemArticlePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemArticleActivity extends AppCompatActivity implements ItemArticleInterface.View{

    private ItemArticlePresenter presenter;

    @BindView(R.id.idToolbar)
    Toolbar toolbar;

    @BindView(R.id.article_title)
    TextView articleTitle;

    @BindView(R.id.article_image)
    ImageView articleImage;

    @BindView(R.id.article_descriprion)
    TextView articleDescription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_article_activity);

        ButterKnife.bind(this);

        initToolbar();
        initUI();
        initPresenter();
        attachView();
    }

    private void initUI() {
        Article article = getIntent().getParcelableExtra(Article.class.getCanonicalName());

        articleTitle.setText(article.getTitle());
        articleDescription.setText(article.getDescription());
        Glide.with(this).load(article.getUrlToImage()).into(articleImage);
    }

    private void initPresenter(){
        presenter = new ItemArticlePresenter();
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detachView();
    }

    protected void attachView() {
        if(presenter != null){
            if(!presenter.isViewAttached()){
                presenter.attachView(this);
            }
        }else{
            initPresenter();
        }
    }

    protected void detachView() {
        if(presenter != null){
            if(presenter.isViewAttached()) {
                presenter.detachView();
            }
        }
    }

    private void initToolbar() {
        toolbar.setNavigationIcon(R.drawable.arrow_left_white);
        toolbar.setTitle(R.string.articles);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setNavigationOnClickListener(v -> this.finish());
    }
}
