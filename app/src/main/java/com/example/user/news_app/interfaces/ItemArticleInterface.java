package com.example.user.news_app.interfaces;

import com.example.user.news_app.interfaces.base.BaseInterfacePresenter;
import com.example.user.news_app.interfaces.base.BaseInterfaceView;

public interface ItemArticleInterface {

    interface View extends BaseInterfaceView {

    }

    interface Presenter extends BaseInterfacePresenter<View> {

    }
}
